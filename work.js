var work = [
    /*기타*/
    { "cate":"m01", "dep01":"회원", "dep02":"아이디 찾기", "url":"./dist/find_id.html", "date":"2022-11-16" },
    { "cate":"m01", "dep01":"회원", "dep02":"아이디 찾기-결과", "url":"./dist/find_id_complete.html", "date":"2022-11-16" },
    { "cate":"m01", "dep01":"회원", "dep02":"아이디 찾기-에러표시", "url":"./dist/find_id_error.html", "date":"2022-11-16" },
    { "cate":"m01", "dep01":"회원", "dep02":"비밀번호 찾기", "url":"./dist/find_password.html", "date":"2022-11-16" },
    { "cate":"m01", "dep01":"회원", "dep02":"비밀번호 찾기-결과", "url":"./dist/find_password_complete.html", "date":"2022-11-16" },
    { "cate":"m01", "dep01":"회원", "dep02":"비밀번호 찾기-에러표시", "url":"./dist/find_password_error.html", "date":"2022-11-16" },
    { "cate":"m01", "dep01":"회원", "dep02":"로그인", "url":"./dist/login.html", "date":"2022-11-16" },
    { "cate":"m01", "dep01":"회원", "dep02":"로그인-에러표시", "url":"./dist/login_error.html", "date":"2022-11-16" },
    { "cate":"m01", "dep01":"회원", "dep02":"[팝업]공통", "url":"./dist/popup.html", "date":"2022-11-16" },
    { "cate":"m01", "dep01":"회원", "dep02":"[팝업]이용약관 동의 (필수)", "url":"./dist/popup_agree_01.html", "date":"2022-11-16" },
    { "cate":"m01", "dep01":"회원", "dep02":"[팝업]개인정보 수집 및 이용 동의 (필수)", "url":"./dist/popup_agree_02.html", "date":"2022-11-16" },
    { "cate":"m01", "dep01":"회원", "dep02":"[팝업]개인정보의 마케팅 및 광고 활용 (선택)", "url":"./dist/popup_agree_03.html", "date":"2022-11-16" },
    { "cate":"m01", "dep01":"회원", "dep02":"회원가입-일반 약관동의", "url":"./dist/signup_01.html", "date":"2022-11-16" },
    { "cate":"m01", "dep01":"회원", "dep02":"회원가입-일반 입력", "url":"./dist/signup_02.html", "date":"2022-11-16" },
    { "cate":"m01", "dep01":"회원", "dep02":"회원가입-일반 입력 - 에러표시", "url":"./dist/signup_02_error.html", "date":"2022-11-16" },
    { "cate":"m01", "dep01":"회원", "dep02":"회원가입-완료", "url":"./dist/signup_03.html", "date":"2022-11-16" },

    { "cate":"m01", "dep01":"회원", "dep02":"회원가입-간편 약관동의", "url":"./dist/signup_11.html", "date":"2022-11-16" },
    { "cate":"m01", "dep01":"회원", "dep02":"회원가입-간편 입력", "url":"./dist/signup_12.html", "date":"2022-11-16" },
    { "cate":"m01", "dep01":"회원", "dep02":"회원가입-간편 입력 - 에러표시", "url":"./dist/signup_12_error.html", "date":"2022-11-16" },

	{ "cate":"m02", "dep01":"결제", "dep02":"주문 / 결제 - 입력", "url":"dist/payment/payment.html", "date":"2022-11-16" },
    { "cate":"m02", "dep01":"결제", "dep02":"주문 / 결제 - 완료", "url":"dist/payment/order-result.html", "date":"2022-11-16" },

    { "cate":"m03", "dep01":"마이페이지", "dep02":"내 정보", "url":"dist/mypage/my-info.html", "date":"2022-11-18" },
    { "cate":"m03", "dep01":"마이페이지", "dep02":"결제 정보", "url":"dist/mypage/payment-info.html", "date":"2022-11-18" },
    { "cate":"m03", "dep01":"마이페이지", "dep02":"회원 탈퇴 - 사유입력", "url":"dist/mypage/out-form.html", "date":"2022-11-18" },
    { "cate":"m03", "dep01":"마이페이지", "dep02":"회원 탈퇴 - 완료", "url":"dist/mypage/out-complete.html", "date":"2022-11-18" },
    { "cate":"m03", "dep01":"마이페이지", "dep02":"[팝업]필수입력 사항 미입력01", "url":"dist/mypage/pop-msg01.html", "date":"2022-11-18" },
    { "cate":"m03", "dep01":"마이페이지", "dep02":"[팝업]필수입력 사항 미입력01", "url":"dist/mypage/pop-msg02.html", "date":"2022-11-18" },

    //{ "cate":"m04", "dep01":"기타", "dep02":"앱 설치 현황", "url":"html/popup/pop-app-status.html", "date":"2022-11-16" },


];

 
$(function(){   
	listTable(".siteNavi li", ".siteNavi li .num");
}); 
 
function listTable(cls, num){   
	var tr;
	for(i=0; i<work.length; i++){
		tr += "<tr class="+work[i].cate+">";
		tr += "<td>"+work[i].dep01+"</td>";
		tr += "<td>"+work[i].dep02+"</td>";
		tr += "<td><a href='./"+work[i].url+"' target='_blank'>"+work[i].url+"</a></td>";
		tr += "<td class='ac'>"+work[i].date+"</td>";
		tr += "</tr>"; 
	}  
	$("table tbody").append(tr);  
	
	$(num).each(function(z){
		if(z===0){
			$(num).eq(z).text("("+work.length+"p)");
		}else{
			$(num).eq(z).text("("+$('.m0'+z).length+"p)");
		} 
	}); 
	$("body").on("click",cls, function(){
		$(cls).removeClass("on"); 
		$(this).addClass("on");
		$("table tbody tr").hide();
		if($(this).index() === 0){
			$("table tbody tr").show();
		}else{
			$(".m0"+$(this).index()).show();
		} 
	});  
}  
